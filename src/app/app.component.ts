import { Component, ViewChildren, OnInit, ChangeDetectorRef, AfterViewInit, QueryList } from '@angular/core';
import { MessageComponent } from './message/message.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  messages: any;
  @ViewChildren(MessageComponent) messageViewChildren: QueryList <MessageComponent>;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.messages = this.getMessages();
  }

  ngAfterViewInit() {
    console.log(this.messageViewChildren);
    this.messageViewChildren.forEach((item) => {
    if (item.msg.startsWith('s')) {
       item.msg = 'Infragistics';
      }
    });
    this.cd.detectChanges();
}

  getMessages() {
    return [
      'first message',
      'second message',
      'third message',
      'fourth message',
      'fifth message',
    ];
  }

}
